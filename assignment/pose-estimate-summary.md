# Pose Estimation
## Overview
The basic idea of Pose Estimation is to track people's movements in some source of input such as video or image.
Pose Estimation works by defining **keypoints** on the subject in case of human body these keypoints can be **wrists ,elbows ,knees and ankles.**
![](../images/overview.png)

## How can some technology detect pose of a human being?
The main aim of Pose Estimation is to detect the coordinates (XYZ) for a specific or specified number of joints on the body using a image or a frame in a video containing a subject.

As the first step of detecting the points gets completed, the movement analysis system checks the posture of the subject and relates each point to the most appropriate neighbouring point hence forming the exact posture of the subject.

## Multiple Approaches to Pose Estimation
* **Training a  model to infer 3D keypoints directly from the provided Images.**
In this approach a multi-view model _EpipolarPose_ is trained to predict or estimate the positions in both 2D as well as 3D. The most intresting thing about this approach is that **it does not requires any ground truth 3D data to train the model** . The 3D ground truth data is constructed in a self-supervised way using epipolar geometry to 3D predictions.
![](../images/epipolar.jpg)
EpipolarPose
* **Detecting the 2D keypoints and then transform them into 3D**
This approach is one of the most commonly used approaches nowadays as 2D keypoint prediction is well-explored also there are pre-trained backbones of 2D prediction which increases the overall accuracy of the system. There are many existing models which provide us with a high accuracy, some of them are: _PoseNet, HRNet, Mask R-CNN_<br/>
![](../images/posenet.png)
PoseNet Architecture

* **Using multi-view image data**
In this approach multiple cameras are set-up in different views which captures the target scene from different angles hence providing us with multi-view image data which can be then rendered into a 3D input.
Using this approach leads to a more accurate result as the cameras provide with imporve depth perception
![](../images/cameras.png)

## Detection and Analysis
Assuming that a complicated multi-camera setup and depth sensors are not available for Pose Estimation. In this case, we can choose any model like _VideoPose3D_  which can work with simple single-view detection program. VideoPose3d is a model which belongs to Convolutional neural networks family(CNNs).
For analysis a set of 2D keypoints are supplied as input which were detected, the 2D detector is pre-trained on a Dataset(ex: COCO dataset).By getting the information from multiple frames taken at different times, the model basically **makes a prediction based on the past data and the future positions of the joints** this allows the model to achieve higher accuracy on the prediction of the current positions of the joints. 
![](../images/human_pose.gif)