# Object Detection 
Object Detection is a technique through which different objects can be detected or acknowledged in an image or a video source and are classified into different categories or are labelled with great accuracy.
Object Detection is an approach which comprise of _recognition, detection and localization_ of the visible objects in the input source. It uses algorithms to acknowledge all the occurences of an object.

![](../images/object.jpg)

## Strategies of Object Detection
Basically Object Detection follows any of the two strategies namely: **The Machine Studying approach** which uses vector machines such as _SVMs_ to classify the objects, the other strategy is **Deep Studying approach** which uses _Convolutional Neural Networks_ to classify object.

## R-CNN
The R-CNN architecture is based on three main modules.<br/>
 The first module extracts many regions using a segmentation algorithm called _Selective Search_ which basically figure outs which part or region the image contains objects.The algorithm scans the while images with different sized windows and looks for adjacent pixels which share colors and textures.<br/>
The Second module consists of _large CNNs_ that extracts fixed length feature vectors from the regions which were obtained from the previous module.The region then undergoes _image wraping_ to have required input size.<br/>
The final or the third module basically classifies each region with category-specific _SVMs_.

A drawback of R-CNNs are that they are slow and less accurate by today's standards. So in today's world Fast R-CNNs,Mask R-CNNs have replaced the need of the old R-CNNs.

![](../images/R_CNN.png)


## YOLO
YOLO or You Only Look Once is one of the most popular Object detection methods today as it is capable of processing real-time videos in less time.
The most updated version of YOLO is YOLOv3 which is considered more accurate over traditional YOLO models.
YOLOv3 predicts bounding boxes over the imageof 3 different scales .The final object detection is processed using **NMS(Non-Max Suppression)**, a method which ignores the boxes which overlaps each other more than a threshold value. In such cases YOLO keeps the bounding boxes with largest confidence values.
YOLO uses multiple independent logistic classifiers for classification.

![](../images/yolo.png)