# Face Detection
Face Detection is a technology through which human faces can be identified or located easily form an input source such as image or a video feed.
Face Detection is a very specific case of _Object-class Detection_ as face detection only focusses on the detection of the faces of people in the feed.

## Techniques
There are many techniques which can be followed to get a very accurate face detection model. Some of the techniques are described below;

* ### Haar Cascades Classifier
Haar Cascades Classifier is one the first ML based cascading classifier which basically fulfils the requirement of fast implementation of the model on devices with low-CPU power for example- cameras and phones.

![](../images/haar.jpg)

Haar Cascades Classifier is trained with many images before using it on the test cases. The training consists of images which are segregated as _Positive Images_ and _Negative Images_. The Positive images are the images which have faces of humans in it whereas the Negative images are the images which do not have faces in it.Haar extracts the info from the images and then uses it to make predictions to the model later. In the training period, Adaboost (a learning method) is used to select the best features from the image. In the end, the classifier rejects all the irrelevant features and this process is continued until the requires accuracy is achieved.<br/>
![](../images/haarfeatures.png)

* ### Histogram of Oriented Gradients(HOG)
Histogram of Oriented Gradient can act as a image descriptor and can produce a very accurate human detector when trained on a good classifier such as SVM(Support Vector Machine).
HOG becomes a good choice over other detectors as it avoids the detection when the face is covered or is partially shown in the feed hence, HOG can be used considering the usage of the application.
![](../images/HOG.png)

* ### Multi-task Cascaded Convolutional Networks (MTCNN)
In MTCNN generally consists of 3 stages of cascading Convolutional Neural Network to train and predict the face with 5 Landmark locations for a proper face alignment.<br/>
![](../images/mtcnn-output.jpeg)
<br/>
The process of face detection through MTCNN follows as:<br/>
**First Step:** In the first step the algorithm predicts bounding boxes around the subjects in the image using regression techniques when passed to _Proposal Network_ also known as _P Net_. NMS or non-maximum-suppresion is used to merge highly overlapped subjects in the image.<br/>
**Second Step:** The ouput from the previous state behaves as an input to a new CNN, known as _R-Net_ where this CNN will also reject many false subjects and makes the prediction of the bounding boxes more accurate, then applying NMS to remove the overlapped boxes.<br/>
**Third Step:** The third step is quite similar to the previous step, where the difference comes in the CNN which is used i.e. _O-Net(Output Network)_ and also this stage predicts _five facial landmark's position_<br/>
![](../images/MTCNN.png)